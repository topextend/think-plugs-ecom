<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-09 23:44:37
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-11 15:52:37
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : EcomPlat.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\ecom\model;

use think\admin\Model;

/**
 * 电商平台列表模型
 * Class EcomPlat
 * @package app\ecom\model
 */
class EcomPlat extends Model
{
    /**
     * 获取电商平台列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getParentData()
    {
        $plats = static::mk()->where(['status' => 1])->column('id, plat_name, plat_desc');
        return $plats;
    }
}