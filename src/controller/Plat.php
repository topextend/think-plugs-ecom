<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-09 23:37:34
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-12 16:07:41
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Plat.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\ecom\controller;

use app\ecom\model\EcomPlat;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 电商平台列表
 * Class Plat
 * @package app\ecom\controller
 */
class Plat extends Controller
{
    /**
     * 电商平台列表
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        EcomPlat::mQuery()->layTable(function () {
            $this->title = "电商平台列表";
        }, function (QueryHelper $query) {
            $query->like('plat_name')->equal('status')->dateBetween('create_at');
        });
    }
    
    /**
     * 添加电商平台
     * @auth true
     */
    public function add()
    {
        EcomPlat::mForm('form');
    }

    /**
     * 编辑电商平台
     * @auth true
     */
    public function edit()
    {
        EcomPlat::mForm('form');
    }
    
    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            // 检查平台名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['plat_name' => $data['plat_name']];
                if (EcomPlat::mk()->where($where)->count() > 0) {
                    $this->error("平台名称 {$data['plat_name']} 已经存在，请使用其它平台名称！");
                }
            }
        }
    }

    /**
     * 修改电商平台状态
     * @auth true
     */
    public function state()
    {
        EcomPlat::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除电商平台
     * @auth true
     */
    public function remove()
    {
        EcomPlat::mDelete();
    }
}