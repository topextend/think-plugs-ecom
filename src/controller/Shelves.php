<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-12 17:39:57
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-17 17:35:22
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Shelves.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\ecom\controller;

use app\ecom\model\EcomShelves;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 店铺上架记录
 * Class Shelves
 * @package app\ecom\controller
 */
class Shelves extends Controller
{
    /**
     * 店铺上架记录
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        EcomShelves::mQuery()->layTable(function () {
            $this->title = '上架商品列表管理';
            $this->shop_id = $this->app->request->get('shop_id');
        }, function (QueryHelper $query) {
            $query->like('name')->where(['shop_id'=>trim(input('get.shop_id'))])->equal('status')->dateBetween('create_at');
        });
    }

    /**
     * 添加上架记录
     * @auth true
     */
    public function add()
    {
        EcomShelves::mForm('form');
    }

    /**
     * 编辑上架记录
     * @auth true
     */
    public function edit()
    {
        EcomShelves::mForm('form');
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            // 检查仓库名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['name' => $data['name']];
                if (EcomWarehous::mk()->where($where)->count() > 0) {
                    $this->error("仓库名称 {$data['name']} 已经存在，请使用其它仓库名称！");
                }
            }
        }
    }

    /**
     * 修改电商仓库状态
     * @auth true
     */
    public function state()
    {
        EcomShelves::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除上架记录
     * @auth true
     */
    public function remove()
    {
        EcomShelves::mDelete();
    }
}