<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-11 20:42:41
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-17 17:55:58
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Warehous.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\ecom\controller;

use app\mall\model\MallCate;
use app\ecom\model\EcomWarehous;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 店铺仓储管理
 * Class Warehous
 * @package app\ecom\controller
 */
class Warehous extends Controller
{
    /**
     * 店铺仓储管理
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        EcomWarehous::mQuery()->layTable(function () {
            $this->title = '仓库列表管理';
            $this->cat_id = $this->app->request->get('cat_id','0');
        }, function (QueryHelper $query) {
            $query->like('name')->where(['cat_id'=>trim(input('get.cat_id'))])->equal('status')->dateBetween('create_at');
        });
    }
    
    /**
     * 添加电商仓库
     * @auth true
     */
    public function add()
    {
        EcomWarehous::mForm('form');
    }

    /**
     * 编辑电商仓库
     * @auth true
     */
    public function edit()
    {
        EcomWarehous::mForm('form');
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            // 检查仓库名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['name' => $data['name']];
                if (EcomWarehous::mk()->where($where)->count() > 0) {
                    $this->error("仓库名称 {$data['name']} 已经存在，请使用其它仓库名称！");
                }
            }
        }
    }

    /**
     * 仓库报单
     * @auth true
     */
    public function report()
    {
        //
    }

    /**
     * 修改电商仓库状态
     * @auth true
     */
    public function state()
    {
        EcomWarehous::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除电商仓库
     * @auth true
     */
    public function remove()
    {
        EcomWarehous::mDelete();
    }
}