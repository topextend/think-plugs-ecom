<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-10 11:40:18
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-17 17:21:10
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Shop.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\ecom\controller;

use app\ecom\model\EcomShop;
use app\ecom\model\EcomPlat;
use app\mall\model\MallCate;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 电商店铺列表
 * Class Shop
 * @package app\ecom\controller
 */
class Shop extends Controller
{
    /**
     * 电商店铺列表
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        EcomShop::mQuery()->layTable(function () {
            $this->title = "电商店铺列表";
            $this->shop_type = ['t0' => '自营店铺', 't1' => '代理店铺'];
            $this->type = $this->app->request->get('type', 't0');
            $this->total = ['t0' => 0, 't1' => 0];
            foreach (EcomShop::mk()->field('shop_type, count(1) total')->group('shop_type')->cursor() as $vo) {
                [$this->total["t{$vo['shop_type']}"] = $vo['total']];
            }
            $this->plats = EcomPlat::getParentData();
            $this->cates = MallCate::treeData();
        }, function (QueryHelper $query) {
            // 加载对应数据列表
            $query->like('shop_name,plat_id,cat_id')->where(['shop_type' => trim(input('get.type', 't0'),'t')]);
        });
    }
    
    /**
     * 列表数据处理
     * @param array $data
     */
    protected function _index_page_filter(array &$data)
    {
        foreach ($data as $k => $vo)
        {
            $data[$k]['plat_name'] = EcomPlat::mk()->where(['id' => $vo['plat_id']])->value('plat_name');
            $data[$k]['cat_name']  = MallCate::mk()->where(['id' => $vo['cat_id']])->value('name');
        }
    }
    
    /**
     * 添加电商店铺
     * @auth true
     */
    public function add()
    {
        EcomShop::mForm('form');
    }

    /**
     * 编辑电商店铺
     * @auth true
     */
    public function edit()
    {
        EcomShop::mForm('form');
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isGet()) {
            $this->plats = EcomPlat::getParentData();
            $this->cates = MallCate::treeData();
        } elseif($this->request->isPost()) {
            // 检查店铺名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['shop_name' => $data['shop_name'], 'plat_id' => $data['plat_id']];
                if (EcomShop::mk()->where($where)->count() > 0) {
                    $this->error("店铺 {$data['shop_name']} 已经存在，请使用其它店铺名称！");
                }
            }
        }
    }
    
    /**
     * 修改电商店铺状态
     * @auth true
     */
    public function state()
    {
        EcomShop::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除电商店铺
     * @auth true
     */
    public function remove()
    {
        EcomShop::mDelete();
    }
}